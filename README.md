# Orientação a Objetos 1/2016

## EP0 - C++

* 1 - Abra a imagem no formato PPM
* 2 - Leia o conteúdo da imagem
* 3 - Salve o conteúdo em outro arquivo

* Mais detalhes na wiki [home](https://gitlab.com/OOFGA-2016-1/EP0/wikis/home)
### Como Compilar e Executar

Para compilar e executar o programa em um sistema operacional Linux, siga as seguintes instruções:

* Abra o terminal;
* Encontre o diretório raiz do projeto;
* Limpe os arquivos objeto:
	**$ make clean** 
* Compile o programa: 
	**$ make**
* Execute:
	**$ make run**
## Como abrir, ler e copiar a imagem PPM

* Compile e execute o programa;
* 1 - Digite o nome da imagem sem a extenção: 
		Ex: Digite imagem se quiser abrir a "imagem.ppm"
* 2 - Digite o endereço completo do diretório aonde se encontra a imagem: 
		Ex: /home/mateus/Downloads/EP0/
* 3 - Se a Imagem for encontrada ela sera lida, caso contrário Ira aparecer os dados requisitados no terminal;
* 4 - Digite o nome final da imagem que vai ser copiada sem a extenção:
		Ex: Digite copy se quiser salvar como "copy.ppm"
* 5 - Digite o endereço completo do diretório aonde será copiada a imagem:
		Ex: /home/mateus/Imagens/
