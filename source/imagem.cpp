#include "ppm.hpp"

Ppm::Ppm(){}

	string Ppm::getMagicNumber(){
		return magicnumber;
	}
	void Ppm::setMagicNumber(string magicnumber){
		this -> magicnumber = magicnumber;
	}
	string Ppm::getPixel(){
		return pixel;
	}
	void Ppm::setPixel(string pixel){
		this -> pixel = pixel;
	}
	int Ppm::getLargura(){
		return largura;
	}
	void Ppm::setLargura(int largura){
		this -> largura = largura;
	}
	int Ppm::getAltura(){
		return altura;
	}
	void Ppm::setAltura(int altura){
		this -> altura = altura;
	}
	int Ppm::getCorMax(){
		return cormax;
	}
	void Ppm::setCorMax(int cormax){
		this -> cormax = cormax;
	}

void Ppm::read(){
	string pixel,magicnumber,allpixels,comentario,mn,c2,c3,c4,c5,archive,im,tudo;
	int largura, altura, cormax;
	char pix;
	cout << "Digite o nome da imagem sem a extensão ";
	cin >> im;
	im = im + ".ppm";
	cout << "Digite o endereço do diretório aonde se encontra a imagem: ";
	cin >> archive;
	tudo = archive + "/" + im;
	ifstream file(tudo.c_str());
	if(!file.is_open()){
		cout << "Não foi possível abrir o arquivo." << endl;
	}
	else{
    

	getline(file,mn);
	if(mn[0]=='#'){
		getline(file,magicnumber);
	}
	else{
		magicnumber=mn;
	}
	setMagicNumber(magicnumber);


	file >> c2;
	if(c2[0]=='#'){
		file >> largura;
	}
	else{
		largura = atoi(c2.c_str());
	}
	setLargura(largura);

	file >> c3;
	if(c3[0]=='#'){
		file >> altura;
	}
	else{
		altura = atoi(c3.c_str());
	}
	setAltura(altura);
	
	file >> c4;
	if(c4[0]=='0'){
		file >> cormax;
	}
	else{
		cormax = atoi(c4.c_str());
	}
	setCorMax(cormax);


	cout << '\n' << Ppm::magicnumber << endl;
	cout << Ppm::largura << "x"<< Ppm::altura << endl;
	cout << Ppm::cormax << '\n' << endl;
	getline(file,allpixels);
		while(file.get(pix)){
		pixel+= pix;
		}
	setPixel(pixel);
	file.clear();
	file.close();
	}
	
}

void Ppm::copy(){
	string cp,arch,fin;
    char blank='\n';
	cout << "Digite o nome final da imagem copiada: ";
	cin >> cp;
	cp = cp + ".ppm";
	cout << "Digite o endereço do diretório aonde sera salva a imagem: ";
	cin >> arch;
	fin = arch + "/" + cp;
    ofstream copy(fin.c_str());
    copy << getMagicNumber();
    copy.put(blank);
    copy << getLargura();
	copy.put(blank);
	copy << getAltura();
    copy.put(blank);
    copy << getCorMax();
    copy.put(blank);
    copy << getPixel();	
	cout << "\n" << "Operação feita com êxito!"<< "\n" << endl;
}
