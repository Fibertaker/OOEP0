#ifndef PPM_HPP
#define PPM_HPP

#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>

using namespace std;

class Ppm{
	private:
		string magicnumber;
		string pixel;
		int largura;
		int altura;
		int cormax;
	public:
		Ppm();
		string getMagicNumber();
		void setMagicNumber(string magicnumber);
		string getPixel();
		void setPixel(string pixel);
		int getLargura();
		void setLargura(int largura);
		int getAltura();
		void setAltura(int altura);
		int getCorMax();
		void setCorMax(int cormax);
		void read();
		void copy();
};

#endif
